from django.apps import AppConfig


class Bancog6P53AppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bancog6p53App'
