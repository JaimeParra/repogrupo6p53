# Generated by Django 3.2.7 on 2021-10-22 02:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bancog6p53App', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='number',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='user',
            name='phone',
            field=models.CharField(default='n/a', max_length=20, unique=True, verbose_name='Phone'),
        ),
    ]
