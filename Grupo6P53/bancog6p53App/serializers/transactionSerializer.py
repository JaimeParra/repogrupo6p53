from bancog6p53App.models import transaction
from bancog6p53App.models import account
from bancog6p53App.models.account import Account
from bancog6p53App.models.user import User
from bancog6p53App.models.transaction import Transaction
from rest_framework import serializers

class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ['date', 'amount', 'type']

    def to_representation(self, obj):
        transaction = Transaction.objects.get(id=obj.id)
        account = Account.objects.get(id=transaction.account_id)        
        return{
            'id' : transaction.id,
            'date' : transaction.date,
            'amount' : transaction.amout,
            'type' : transaction.type,
            'account' : {
                'id' : account.id,
                'balance' : account.balance,
            }
        },
  