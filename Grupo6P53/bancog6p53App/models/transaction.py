from django.db import models
from .account import Account
from datetime import datetime, timedelta

class Transaction(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(default=datetime.now() + timedelta(days=1))
    account = models.ForeignKey(Account, related_name='transaction', on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
    type = models.CharField('Type', max_length = 50)
    